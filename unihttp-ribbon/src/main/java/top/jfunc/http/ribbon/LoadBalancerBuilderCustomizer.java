package top.jfunc.http.ribbon;

import com.netflix.loadbalancer.LoadBalancerBuilder;
import com.netflix.loadbalancer.Server;

public interface LoadBalancerBuilderCustomizer<T extends Server> {
    void config(LoadBalancerBuilder<T> builder);
}
