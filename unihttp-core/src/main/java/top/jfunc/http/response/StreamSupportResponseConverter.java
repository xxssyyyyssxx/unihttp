package top.jfunc.http.response;

import java.io.IOException;

public class StreamSupportResponseConverter implements ClientHttpResponseConverter<ClientHttpResponse>{
	public static final StreamSupportResponseConverter INSTANCE = new StreamSupportResponseConverter();

	@Override
	public ClientHttpResponse convert(ClientHttpResponse clientHttpResponse, String resultCharset) throws IOException {
		return clientHttpResponse;
	}

	@Override
	public boolean supportStream() {
		return true;
	}
}
